<?php
/**
 * @package    Joomla.Site
 * @copyright  Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/* The following line loads the MooTools JavaScript Library */
JHtml::_('behavior.framework', true);

/* The following line gets the application object for things like displaying the site name */
$app = JFactory::getApplication();
?>
<?php echo '<?'; ?>xml version="1.0" encoding="<?php echo $this->_charset ?>"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
  <head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="viewport" content="width=device-width">
    <!-- The following JDOC Head tag loads all the header and meta information from your site config and content. -->
    <jdoc:include type="head" />

    <!-- The following five lines load the Blueprint CSS Framework (http://blueprintcss.org). If you don't want to use this framework, delete these lines. -->
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/blueprint/screen.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/blueprint/print.css" type="text/css" media="print" />
    
    <!--[if lt IE 8]><link rel="stylesheet" href="blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/blueprint/plugins/fancy-type/screen.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/blueprint/plugins/joomla-nav/screen.css" type="text/css" media="screen" />

    <!-- The following line loads the template CSS file located in the template folder. -->
    <!--link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/bootstrap.min.css" type="text/css" /-->
   <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />

    <link href='http://fonts.googleapis.com/css?family=Vidaloka' rel='stylesheet' type='text/css'>

    <!-- The following four lines load the Blueprint CSS Framework and the template CSS file for right-to-left languages. If you don't want to use these, delete these lines. -->
    <?php if($this->direction == 'rtl') : ?>
      <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/blueprint/plugins/rtl/screen.css" type="text/css" />
      <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template_rtl.css" type="text/css" />
    <?php endif; ?>

    <!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/template.js"></script>
  </head>
  <body>
<div class="wrapper">
     <div class="header">
          <div class="header_inner">
               <div class="header_inner_left">
                    <div class="logo">
                    <jdoc:include type="modules" name="logo" style="none" />  <!--- LOGO MODULE ----->
                    </div>
               </div>
               
               <div class="header_inner_right">
                                  <div class="login_btn"><jdoc:include type="modules" name="atomic-btlogin" style="none" />                               
                          </div>
                    <jdoc:include type="modules" name="socialicon" style="none" />  <!--- SOCIAL ICON & LOGIN REGISTER LINK  MODULE ----->
                   </div>
                 
          </div>
          
          <div class="clear"></div>

          <div class="menu">
               <div class="menu_inner"> <a href="#" class="toggleMenu" style="display: none;"><em>Menu</em><span></span></a>
               <ul class="nav">
               <jdoc:include type="modules" name="Menu" style="none" />  <!--- MENU MODULE ----->
               </ul>
               </div>
			         <div class="searchbar"> <jdoc:include type="modules" name="search" style="none" /></div> 
          </div>
     </div>
<!--- search box HERE ----->
     
      <div class="main_container">
           <div class="container">
                <div class="container_left">
                     <div class="slider">
                    <jdoc:include type="modules" name="Slider" style="none" />  <!--- SLIDER MODULE ----->
                     </div>
                     
                     <div class="container_left_box">
                         <jdoc:include type="component" /> <!--- CONTENT HERE ----->
                     </div>
                     
                     <div class="container_left_box2">
                         <jdoc:include type="modules" name="whatnew" style="none" /><!--- What's New CONTENT  HERE ----->
                     </div>
                     <div class="clear"></div>
                     
                     <div class="bottom_box">
                          <div class="bottom_box_left">
                           <jdoc:include type="modules" name="Performances" style="none" /> <!--- Performances CONTENT  HERE ----->
                          </div>
                          
                          
                          <div class="bottom_box_left">
                         <jdoc:include type="modules" name="Recognition" style="none" /> <!--- Recognition CONTENT  HERE ----->
                       </div>
                     </div>
                </div>
                
                <div class="container_right">
                      <div class="right_links">
                      <jdoc:include type="modules" name="right_links" style="xhtml" /> 
                      </div>
                     <div class="container_right_box">
                          <div class="container_right_box_img">
                          <jdoc:include type="modules" name="Music" style="xhtml" /> <!--- Music CONTENT  HERE ----->
                        </div>
                     </div>
                     
                     <div class="container_right_box">  <div class="container_right_box_img">
                       <jdoc:include type="modules" name="stage" style="xhtml" /> <!--- Film/Stage CONTENT  HERE ----->
                          </div>
                     </div>
                     <div class="container_right_box">
                         <div class="container_right_box_img">
                          <jdoc:include type="modules" name="Comedy" style="xhtml" /> <!--- Comedy CONTENT  HERE ----->
                     </div>
                     </div>
                     <div class="container_right_box">
                      
                          <div class="container_right_box_img">
                          <jdoc:include type="modules" name="Fashion" style="xhtml" /> <!--- Fashion CONTENT  HERE ----->
                          </div>
                     </div>
                         <div class="container_right_box">
                         <div class="container_right_box_img"> 
                          <jdoc:include type="modules" name="Other" style="xhtml" /> <!--- OTHER CONTENT  HERE ----->
                          </div>
                     </div>
                     <div class="container_right_box" style="height: 321px;">
                         
                            <jdoc:include type="modules" name="facebook" style="xhtml" /> <!--- facebook CONTENT  HERE ----->
                          
                     </div>
                </div>
           </div>
           <div class="footer">
         <div class="footer_inner">
            <div class="footer_about">
               <div class="footer_about_inner">
                 <jdoc:include type="modules" name="about" style="none" /> <!--- about CONTENT  HERE ----->
               </div>
            </div>
            <div class="twitter">
            <div class="twitter_inner">
              <h1>Sponsors</h1>
               <jdoc:include type="modules" name="TWITTER" style="none" /> <!--- TWITTER CONTENT  HERE ----->
              </div>
            </div>
            <div class="quicklinks">
               <div class="quicklinks_inner">
                  <h1>Quick Links</h1>
                  <ul>
                    <li style="color: #ca4753;font-size: 19px;"><a href="http://ksafoundation.org/">ksafoundation.org</a></li>
                  </ul>
                                    
               </div>
            </div>
               </div>
                <div class="copyright">
                  <div class="copyright_inner">
                    <p>Copyright © 2014- Africa Boku Talent.com All Rights Reserved. Site design and hosting by <a target = "_blank" href="http://www.toredit.com">Tored IT</a></p>
                 
                  </div>
                </div>
         </div>
      </div>
      </div>
      
</div>
<div style="display:none;">
  <jdoc:include type="modules" name="hideenmenu" style="none" />  <!--- HIDDEN MENU ----->
</div>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/script.js"></script>
</body>
</html>